import React from 'react'
import { Button } from 'antd'
import { PlusCircleFilled } from '@ant-design/icons'

// import LandingPage from './components/LandingPage'

const App = () => {
  return (
    <div>
      <Button 
        type='primary' 
        icon={<PlusCircleFilled/>}
        data-testid='add-contact-button'
      >
        Hi world
      </Button>
    </div>
  )
}

export default App
